import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import home from '@/components/home'
import welcome from '@/components/welcome'
import users from '@/components/users/user'

Vue.use(Router)

const router= new Router({
  mode:'history',
  routes: [
    {path: '/',redirect:'/login'},
    {
      path: '/login',
      name:'login',
      component: login
    },
    {
      path: '/home',
      name: 'home',
      component: home,
      redirect: '/welcome',
      children:[{
        path:'/welcome',name:welcome,component:welcome},
        {path:'/users',component:users}
      ]
    }
  ]
})
router.beforeEach((to, from, next) =>{
  if(to.path==='/login')return next()
  //获取token
  const tokenStr=window.sessionStorage.getItem('token')
  if(!tokenStr) return next('/login')
  next()
} )
export default router
